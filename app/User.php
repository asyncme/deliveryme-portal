<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;

class User extends Authenticatable
{

  protected $table = 'person';

  protected $primaryKey = 'personId';

  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      //'name', 'email', 'password',
      'email', 'password',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [
      //'password', 'remember_token',
  ];

  public function getAuthPassword() {

    return $this->password;

  }

  //association 

  public function companyMap() {
    return $this->hasOne('App\CompanyMap', 'personId', 'personId');
  }

}
