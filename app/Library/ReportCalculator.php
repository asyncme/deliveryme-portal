<?php
namespace App\Library {

	use App\Setting;
	use App\CompanyMap;
	use Log;

	class ReportCalculator {

		public static function getReport($data) {

			$settings = Setting::find(CompanyMap::getCurrentCompanyId());

			$hour_in_seconds = 3600;
			$minute_in_seconds = 3600;

			$default_start_chunks = explode(':', $settings['normalStartTime']);
			$default_end_chunks = explode(':', $settings['normalEndTime']);
			$threshold_break_hour = $settings['breakThreshold'] / $hour_in_seconds;
			$break_length = (float)($settings['breakLength'] / $hour_in_seconds);

			$default_start_hour = $default_start_chunks[0];
			$default_end_hour = $default_end_chunks[0];

			$result = [
				'report' => []
			];

			foreach ($data as $personId => $item) {

				$cost_default = 0;
				$cost_not_default = 0;
				$cost_saturday = 0;
				$cost_sunday = 0;
				$cost_total = 0;

				$hours_default = 0;
				$hours_saturday = 0;
				$hours_sunday = 0;
				$hours_not_default = 0;
				$hours_total = 0;

				foreach ($item as $the_item) {

					$worked_hours = $the_item['end_time'] - $the_item['start_time'];
					$hours = $worked_hours / $hour_in_seconds;
					$minutes_remaining = ($worked_hours % $hour_in_seconds) / $minute_in_seconds;

					$loop_count = 0;

					for ($hourlytime = ($the_item['start_time'] + $hour_in_seconds); $hourlytime <= $the_item['end_time']; $hourlytime += $hour_in_seconds) {


						$loop_count++;
						$the_hour = (int)date('G', $hourlytime);
						$the_day = date('l', $hourlytime);

						$the_rate;

						if ($the_day === 'Saturday') {

							$hours_saturday++;

							$the_rate = $the_item['wage'] * $settings['saturdayRate'];

							$cost_saturday += $the_rate;

						} else if ($the_day === 'Sunday') {

							$hours_sunday++;

							$the_rate = $the_item['wage'] * $settings['sundayRate'];

							$cost_sunday += $the_rate;

						} else if ($the_hour > $default_start_hour && $the_hour < $default_end_hour) {

							$hours_default++;

							$the_rate = $the_item['wage'];
							$cost_default += $the_rate;

						} else if ($loop_count > 10) {

							//increase rate if the count exceeds 10 hours

							$hours_not_default++;

							$the_rate = $the_item['wage'] * $settings['afterHourRate'];

							$cost_not_default += $the_rate;

						} else {

							$hours_not_default++;

							$the_rate = $the_item['wage'] * $settings['afterHourRate'];

							$cost_not_default += $the_rate;

						}

						//deduct breaks

						if ($loop_count == $threshold_break_hour) {

							if ($the_day === 'Saturday') {

								$hours_saturday -= $break_length;

								$the_rate -= ($the_item['wage'] * $settings['saturdayRate']) / $break_length;

								$cost_saturday += $the_rate;

							} else if ($the_day === 'Sunday') {

								$hours_sunday -= $break_length;

								$the_rate -= ($the_item['wage'] * $settings['sundayRate']) / $break_length;

								$cost_sunday += $the_rate;

							} else if ($the_hour > $default_start_hour && $the_hour <= $default_end_hour) {

								$hours_default -= $break_length;

								$the_rate -= $the_item['wage'] / $break_length;

								$cost_default += $the_rate;

							} else if ($loop_count > 10) {

								$hours_not_default -= $break_length;

								$the_rate -= $the_item['wage'] * $settings['afterHourRate'];

								$cost_not_default += $the_rate;

							}else {

								$hours_not_default -= $break_length;

								$the_rate -= $the_item['wage'] * $settings['afterHourRate'];

								$cost_not_default += $the_rate;
								
								// $hours_default -= $break_length;
								//
								// $the_rate -= $the_item['wage'] / $break_length;
								//
								// $cost_default += $the_rate;

							}

							$hours_total += $break_length;
							$cost_total += $the_rate;

						} else {

							$hours_total++;
							$cost_total += $the_rate;

						}

					}

					//add if there are minutes remaining

					if ($minutes_remaining != 0) {

						//$the_hour = (int)date('G', $hourlytime);
						//$the_day = date('l', $hourlytime);
						$the_hour = (int)date('G', $the_item['end_time']);
						$the_day = date('l', $the_item['end_time']);

						$the_rate;

						if ($the_day === 'Saturday') {

							$hours_saturday += $minutes_remaining;

							$the_rate = ($the_item['wage'] * $settings['saturdayRate']) * $minutes_remaining;

							$cost_saturday += $the_rate;

						} else if ($the_day === 'Sunday') {

							$hours_sunday += $minutes_remaining;

							$the_rate = ($the_item['wage'] * $settings['sundayRate']) * $minutes_remaining;

							$hours_sunday++;

							$cost_sunday += $the_rate;

						} else if ($loop_count > 10) {

							//increase rate if the count exceeds 10 hours

							$hours_not_default += $minutes_remaining;

							$the_rate = ($the_item['wage'] * $settings['afterHourRate']) * $minutes_remaining;

							$cost_not_default += $the_rate;

						} else if ($the_hour > $default_start_hour && $the_hour <= $default_end_hour) {

							//$hours_default++;
							$hours_default += $minutes_remaining;

							$the_rate = $the_item['wage'] * $minutes_remaining;

							$cost_default += $the_rate;

						} else {

							$hours_not_default += $minutes_remaining;

							$the_rate = ($the_item['wage'] * $settings['afterHourRate']) * $minutes_remaining;

							$cost_not_default += $the_rate;

						}

						$hours_total += $minutes_remaining;
						$cost_total += $the_rate;

					}

				}

				$result['report'][$personId] = [
					'name' => $the_item['name'],
					'businessHoursWage' => $settings->currency->iso . ' ' . number_format($the_item['wage'], 2, '.', ','),
					'afterHoursWage' => $settings->currency->iso . ' ' . number_format($the_item['wage'] * $settings['afterHourRate'], 2, '.', ','),
					'saturdayWage' => $settings->currency->iso . ' ' . number_format($the_item['wage'] * $settings['saturdayRate'], 2, '.', ','),
					'sundayWage' => $settings->currency->iso . ' ' . number_format($the_item['wage'] * $settings['sundayRate'], 2, '.', ','),
					'businessHours' => number_format($hours_default, 2, '.', ','),
					'afterHours' => number_format($hours_not_default, 2, '.', ','),
					'saturdayHours' => number_format($hours_saturday, 2, '.', ','),
					'sundayHours' => number_format($hours_sunday, 2, '.', ','),
					'totalHours' => number_format($hours_total, 2, '.', ','),
					'businessHoursCost' => $settings->currency->iso . ' ' . number_format($cost_default, 2, '.', ','),
					'afterHoursCost' => $settings->currency->iso . ' ' . number_format($cost_not_default, 2, '.', ','),
					'saturdayCost' => $settings->currency->iso . ' ' . number_format($cost_saturday, 2, '.', ','),
					'sundayCost' => $settings->currency->iso . ' ' . number_format($cost_sunday, 2, '.', ','),
					'totalCost' => $settings->currency->iso . ' ' . number_format($cost_total, 2, '.', ','),
				];

			}

			return $result;

		}

	}

}
