<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{

	protected $table = 'route';
	protected $primaryKey = 'routeId';

	public $timestamps = false;

	public static function getRouteById($routeId){

			$route = Route::where('routeId','=',$routeId)
				->first();

			return $route;
	}

}
