<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteMap extends Model
{

	protected $table = 'routeMap';
	protected $primaryKey = 'routeMapId';

	public $timestamps = false;

	public static function getRouteMapById($routeId){

			$route = Route::where('routeId','=',$routeId)
				->first();

			return $route;
	}

	public static function removeAll($routeId){

		RouteMap::where('routeId',$routeId)
			->update(['active' => 0]);
		
	}

}
