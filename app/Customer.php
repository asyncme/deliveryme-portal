<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CompanyMap;
use App\Company;
use Log;

class Customer extends Model
{

	protected $table = 'customer';

	protected $primaryKey = 'customerId';

	public $timestamps = false;

	public static function getCustomerWithActiveLabel() {

		$customers = Customer::where('companyId','=',CompanyMap::getCurrentCompanyId())
			->where('customer.active','=',1)
			->orderBy('customer.name')
			->get();

			return $customers;

	}

	public static function _decryptStr($str) {

		$key = 'guess the key mate if you can';
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($str), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return $decrypted;

	}

}
