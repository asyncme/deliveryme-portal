<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyMap extends Model
{

	protected $table = 'companyMap';
	protected $primaryKey = 'companyMapId';
	public $timestamps = false;

	public function getCompanyMap(Request $request) {

		return CompanyMap::find($request->id);

	}

	public static function getCurrentCompanyId() {

		$logged_user = \Auth::user();
		
		//single company access only: consider alternative when implementing multiple company per user
		$company = CompanyMap::where('personId', $logged_user->personId)->first();

		return $company->companyId;

	}

	// association *authenticatable
	public function user() {
    return $this->hasOne('App\User', 'personId', 'personId');
  }

}
