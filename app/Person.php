<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CompanyMap;
use App\Company;
use Log;

class Person extends Model
{

	protected $table = 'person';

	protected $primaryKey = 'personId';

	public $timestamps = false;

	//active status value indicator
	public static $activeStatus = 1;
	public static $inactiveStatus = 0;
	public static $deletedStatus = 2;

	//person type value indicator
	public static $person = 1;
	public static $nonPerson = 0;

	public static function getPersonWithActiveLabel() {

		$logged_user = \Auth::user();

		//single company access only: consider alternative when implementing multiple company per user
		$companyMap = CompanyMap::where('personId', $logged_user->personId)->first();
		//Get company admin
		$company = Company::where('companyId',$companyMap->companyId)->first();

		$persons = Person::where('person.active','=', 1)
												->join('companyMap','person.personId','=','companyMap.personId')
												->where('companyMap.companyId', CompanyMap::getCurrentCompanyId())
												->whereIn('companyMap.active', [Person::$activeStatus, Person::$inactiveStatus])
												->get();

		foreach ($persons as $person) {

			$email_template = 'mailto:%s?subject=Your Deliveryme Account has been created&body=Welcome to Deliveryme, %s. Here is your detail.<br>username: %s<br>password: %s';

			//$person->maillink = sprintf($email_template, $person->email, $person->name, $person->email, Person::_decryptStr($person->password));
			$person->maillink = 'mailto:' . $person->email . '?subject=Your Deliveryme Account has been created&body=Welcome to Deliveryme, ' . $person->name . '. Here is your detail.%0D%0A%0D%0Ausername: ' . $person->email . '%0D%0Apassword: ' . Person::_decryptStr($person->password);

			if ($person->active == Person::$activeStatus) {

				$person->activeLabel = 'Active';

			} else {

				$person->activeLabel = 'Inactive';

			}

			//Set if user is admin of the company
			if ($person->personId == $company->adminId) {
				$person->admin = true;
			}else{
				$person->admin = false;
			}

		}

		return $persons;

	}

	public static function _decryptStr($str) {

		$key = 'guess the key mate if you can';
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($str), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return $decrypted;

	}

}
