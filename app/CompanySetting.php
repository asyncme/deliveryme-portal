<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySetting extends Model
{

	protected $table = 'companySetting';

	protected $primaryKey = 'settingId';

	public $timestamps = false;

	//default
	//public static $companyId = 1;
	public static $defaultCurrencyId = 10;

	//association
	public function person() {

		return $this->hasOne('App\Person', 'personId', 'adminId');

	}

}
