<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Person;
use App\Company;
use App\CompanyMap;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use URL;
use Mail;

class CustomersController extends Controller
{

  public function __construct() {
      $this->middleware('auth', ['except' => ['emailvalidation']]);
  }

	public function index(Request $request) {

		$customers = Customer::getCustomerWithActiveLabel();

    return view('customers', [
      'page_title' => 'Customers',
      'customers' => $customers,
    ]);

	}

	public function save(Request $request) {

		$customer = new Customer();
		$customer->name = $request->name;
		$customer->mobile = $request->mobile;
		$customer->address = $request->address;
    $customer->companyId = CompanyMap::getCurrentCompanyId();
    $customer->active = 1;

    // Get lat lng from external request
    $location = $this->getAddressFromString($request->address);
    $customer->lat = $location['lat'];
    $customer->lng = $location['lng'];

		$customer->save();

		return redirect()->back()->with(['result' => 'The customer has been saved.', 'resultstatus' => 'success']);

	}

	public function update(Request $request) {

		$customer = Customer::find($request->customerId);
		$customer->name = $request->name;
		$customer->mobile = $request->mobile;
		$customer->address = $request->address;

    $location = $this->getAddressFromString($request->address);
    $customer->lat = $location['lat'];
    $customer->lng = $location['lng'];

		$customer->save();

		return redirect()->back()->with(['result' => 'The customer change has been saved.', 'resultstatus' => 'success']);

	}

	public function delete(Request $request) {

		$customer = Customer::find($request->customerId);
		$customer->active = 0;
		$customer->save();

		//return redirect()->action('PersonsController@index', ['result' => 'deletesuccess']);
		return redirect()->back()->with(['result' => 'The customer has been deleted.', 'resultstatus' => 'success']);

	}

	private function _customEncrypt ($str){

		$key = 'guess the key mate if you can';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));

		return $encrypted;

	}

	//Workaround wrapper for setting session flash, as the default session flash does not work
	private function _setSessionFlash($flag) {

		if ($flag == 'savesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The person has been saved.',
			]);

		} else if ($flag == 'updatesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The change has been saved.',
			]);

		} else if ($flag == 'deletesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The person has been deleted.',
			]);

		} else if ($flag == 'savefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The person could not be added. Please try again.'
			]);

		} else if ($flag == 'updatefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The change could not be saved. Please try again.'
			]);

		} else if ($flag == 'deletefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The person could not be deleted. Please try again.'
			]);

		}

	}

  public function register(Request $request) {
    dd($request);
    //Need to validate the inforations

		$person = new Person();

		$person->name = $request->name;
		$person->email = $request->email;
		$person->mobile = $request->mobile;
		$person->password = bcrypt($request->password);
		//$person->password = $this->_customEncrypt($request->password);
		// $person->isPerson = Person::$person;
		$person->active = Person::$activeStatus;

		$person->save();

    if ($person->personId) {

      //Save company
      $company = new Company();
      $company->companyName = $request->company;
      $company->adminId = $person->personId;
      $company->timezone = "Australia/Melbourne";

      $company->save;

      if ($company->companyId) {

        //Save person to companyMap
        //Need to save the company id when user login
        $companyMap = new CompanyMap();
        $companyMap->companyId = $company->companyId;
        $companyMap->personId = $person->personId;
        $companyMap->driver = 1;

        $companyMap->save();

        $mail_data = [
    			'title' => 'Person saved successfuly.',
    			'name' => $request->name,
    			'email' => $request->email,
    			'password' => $request->password,
    		];

        return redirect()->action('login', ['result' => 'savesuccess']);

        Mail::send('email_register', $mail_data, function($m) use ($person) {

    			$m->from('noreply@asyncme.com');
    			$m->subject('Welcome to DeliveryMe');
    			$m->to($person->email);

    		});

      }else{

        return redirect()->action('register', ['result' => 'failed']);

      }

    }else{

      return redirect()->action('register', ['result' => 'failed']);

    }

	}

	public function emailvalidation(Request $request) {

		$result = [];

		if ($request->email) {

			if ($request->exception_id) {

				$existing_emails = Person::where('email', $request->email)
					->whereNotIn('personId', [$request->exception_id])
					->first();

			} else {

				$existing_emails = Person::where('email', $request->email)->first();

			}

			if (!$existing_emails) {

				$result = [
					'valid' => true
				];

			} else {

				$result = [
					'valid' => false,
					'message' => 'The email already exists. Please use a different email address.',
				];

			}

		}

		return response()->json($result);

	}

  private function _encryptStr($str){

    $key = 'guess the key mate if you can';
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));
    return $encrypted;

  }

  public function resendEmail(){

    $person = Person::find($_POST['id']);

    $mail_data = [
      'title' => 'Welcome to DeliveryMe.',
      'name' => $person->name,
      'email' => $person->email,
      'password' =>  Person::_decryptStr($person->password),
    ];

    Mail::send('email_welcome', $mail_data, function($m) use ($person) {

			$m->from('noreply@asyncme.com');
			$m->subject('Welcome to DeliveryMe');
			$m->to($person->email);

		});

    return redirect('/persons')->with(['result' => 'Email sent successfuly.', 'resultstatus' => 'success']);

  }

  public function followupEmail(Request $request){

    // 4 - phsgjc@gmail.com
    $person = Person::find($request->customerId);

    $mail_data = [
      'title' => 'Paul from DeliveryMe',
      'name' => $person->name,
      'email' => $person->email,
    ];

    Mail::send('email_followup_1', $mail_data, function($m) use ($person) {

      $m->from('paul@asyncme.com');
      $m->subject('Paul from DeliveryMe');
      $m->to($person->email);

      print_r('Email sent');
    });

  }

  function getAddressFromString($string){

  	$string = str_replace (" ", "+", urlencode($string));
  	$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, $details_url);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  	$response = json_decode(curl_exec($ch), true);

  	// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
  	if ($response['status'] != 'OK') {
  	 return null;
  	}

  	$location = array();
  	foreach ($response['results'][0]['address_components'] as $component) {

  		switch ($component['types']) {
  			case in_array('street_number', $component['types']):
  			 	$location['street_number'] = $component['long_name'];
  			 	break;
  			case in_array('route', $component['types']):
  				$location['street_name'] = $component['long_name'];
  				break;
  			case in_array('sublocality', $component['types']):
  				$location['sublocality'] = $component['long_name'];
  				break;
  			case in_array('locality', $component['types']):
  				$location['locality'] = $component['long_name'];
  				break;
  			case in_array('administrative_area_level_2', $component['types']):
  				$location['admin_2'] = $component['long_name'];
  				break;
  			case in_array('administrative_area_level_1', $component['types']):
  				$location['admin_1'] = $component['long_name'];
  				break;
  			case in_array('postal_code', $component['types']):
  				$location['postal_code'] = $component['long_name'];
  				break;
  			case in_array('country', $component['types']):
  				$location['country'] = $component['long_name'];
  				break;
  			case in_array('lat', $component['types']):
  	 			$location['lat'] = $component['long_name'];
  	 			break;
  	 	}

  	}

  	$location['lat'] = $response['results'][0]['geometry']['location']['lat'];
   	$location['lng'] = $response['results'][0]['geometry']['location']['lng'];

  	return $location;

  }

}
