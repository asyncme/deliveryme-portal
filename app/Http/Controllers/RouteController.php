<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Person;
use App\Company;
use App\Customer;
use App\Route;
use App\RouteMap;
use App\CompanyMap;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Log;

class RouteController extends Controller
{

	public function index(Request $request) {

    $routes = Route::where('route.companyId','=', CompanyMap::getCurrentCompanyId())
      ->select('person.name as personName','route.*')
      ->join('person','person.personId','=','route.personId')
      ->where('route.active','=',1)
			->orderBy('route.routeId', 'desc')
      ->get();

		$arrayRoute = array();
		$p = 0;
		foreach ($routes as $r) {
			$routeMap = RouteMap::where('routeId',$r->routeId)
				->join('customer','customer.customerId','=','routeMap.customerId')
				->where('routeMap.active','=',1)
				->get();
			$arrayRoute[$p]['route'] = $r;
			$arrayRoute[$p]['routeMap'] = $routeMap;
			$p++;
		}

    $persons = Person::getPersonWithActiveLabel();
		$customers = Customer::getCustomerWithActiveLabel();

    return view('routes', [
  		'page_title' => 'Route Schedule',
  		'routes' => $arrayRoute,
      'persons' => $persons,
			'customers' => $customers
  	]);

	}

  public function save(Request $request){
		// dd($request);
		$company = CompanyMap::getCurrentCompanyId();
		$timezone = config('app.timezone');

		$date = Carbon::createFromFormat('d-m-Y H:i A', $request->startDate . " " . $request->startTime, $timezone);

    $route = new Route();
    $route->name = $request->name;
		$route->time = $date->timestamp;
		$route->timezone = $timezone;
		$route->companyId = $company;
		$route->personId = $request->driver;
		$route->save();

		$list = $request->list;

		//Save routeMap
		for ($i=0; $i < count($list); $i++) {

			$routeMap = new RouteMap();
			$routeMap->routeId = $route->routeId;
			$routeMap->customerId = $list[$i];
			$routeMap->order = $i;

			//Loop notes to find note for user on route
			$notes = json_decode($request->notes);
			foreach ($notes as $note) {
				if ($note->customerId == $routeMap->customerId) {
					$routeMap->note = $note->note;
					break;
				}
			}

			$routeMap->save();

		}

		return redirect()->back()->with(['result' => 'Route saved successfully.', 'resultstatus' => 'success', 'routeId' => $route->routeId]);

  }

	public function update(Request $request){
		$timezone = config('app.timezone');

		$date = Carbon::createFromFormat('d-m-Y H:i A', $request->startDate . " " . $request->startTime, $timezone);

    $route = Route::find($request->routeId);
    $route->name = $request->name;
		$route->time = $date->timestamp;
		$route->personId = $request->driver;
		$route->save();

		//Remove all from RouteMap and re add them
		RouteMap::removeAll($request->routeId);

		$list = $request->list;

		//Save routeMap
		for ($i=0; $i < count($list); $i++) {
			$routeMap = new RouteMap();
			$routeMap->routeId = $route->routeId;
			$routeMap->customerId = $list[$i];
			$routeMap->order = $i;

			$notes = json_decode($request->notes);
			foreach ($notes as $note) {
				if ($note->customerId == $routeMap->customerId) {
					$routeMap->note = $note->note;
					break;
				}
			}

			$routeMap->save();

		}

		return redirect()->back()->with(['result' => 'Route updated successfully.', 'resultstatus' => 'success', 'url' => 'routes']);

  }

	public function delete(Request $request){

		$route = Route::getRouteById($request->routeId);
		$route->active = 0;
		$route->save();

		return redirect()->back()->with(['result' => 'Route deleted successfully.', 'resultstatus' => 'success']);

	}

}
