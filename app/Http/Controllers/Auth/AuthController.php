<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Person;
use App\Company;
use App\CompanyMap;
use App\Setting;
use App\Order;
use Validator;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\RedirectUsers;
/*
*/

class AuthController extends Controller
{

    use \Illuminate\Foundation\Auth\RedirectsUsers;

    protected $redirectPath = '/routes';

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return $this->showLoginForm();
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (property_exists($this, 'loginView')) {
            return view($this->loginView);
        }

        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        return $this->login($request);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        /*
        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }
        */

        /*
        * Temporary use of custom attempt function. Need modification for better practice
        */
        if ($this->_attempt($credentials)) {
          return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    private function _attempt($credentials) {

      $result = false;

      $user = User::where('person.email', $credentials['email'])
        ->where('person.password', $this->_encryptStr($credentials['password']))
        ->first();

      if ($user && $user->companyMap->accessLevel != Company::$normalUser) {

        Auth::login($user, true);
        $result = true;

      }

      return $result;

    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
                ? Lang::get('auth.failed')
                : 'These credentials do not match our records.';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        return $this->logout();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return string|null
     */
    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }


    /*********
    * Register Facede
    *********/
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return $this->showRegistrationForm();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
      return $this->register($request);
    }

    /**
    * Handle a registration request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {

      $person = new Person();

      $person->name = $request->name;
      $person->email = $request->email;
      $person->mobile = "";
      $person->password = $this->_encryptStr($request->password);
      // $person->isPerson = Person::$person;
      $person->active = Person::$activeStatus;

      $person->save();

      if ($person->personId) {

        //Save company
        $company = new Company();
        $company->companyName = $request->company;
        $company->adminId = $person->personId;

        $company->save();

        if ($company->companyId) {

          //Save person to companyMap
          //Need to save the company id when user login
          $companyMap = new CompanyMap();
          $companyMap->companyId = $company->companyId;
          $companyMap->personId = $person->personId;
          $companyMap->accessLevel = Company::$adminUser;

          $companyMap->save();

          $timezone = 'Australia/Melbourne';
          $dt = new \DateTime();
          $dt->setTimezone(new \DateTimeZone($timezone));

          //Safe company default settings
          $setting = new Setting();
          $setting->companyId = $company->companyId;
          $setting->adminId = $person->personId;
          $setting->currencyId = 10; //Australian Dollar as default
          $setting->packageId = 3; //Set comapny with full access by default
          $setting->packageDueDate = $dt->getTimestamp() + 2592000; //Due date is today + 30 days
          $setting->normalStartTime = '09:00:00'; // Start at 9AM
          $setting->normalEndTime = '17:00:00'; // End at 5 PM
          $setting->afterHourRate = 1.0; // Rate for after hour work
          $setting->saturdayRate = 1.0; // Rate for saturday's work
          $setting->sundayRate = 1.0; // Rate for sunday's work
          $setting->maxLength = 8; // Alert user about his clockin time
          $setting->breakLength = 1800; //Amount of seconds to be discounted ( 1800 = 30 min )
          $setting->breakThreshold = 28800; // Time duration from when the discount will happen ( 28800 = 8 hours)

          $setting->save();

          //Save first order for comapny with amout 0
          $order = new Order();
          $order->companyId = $company->companyId;
          $order->pkgId =  3; // 3 is the pro package
          $order->time = $dt->getTimestamp(); // get current timezone
          $order->timezone = $timezone; //Get browser timezone

          $order->save();

          $mail_data = [
            'title' => 'Person saved successfuly.',
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
          ];

          Mail::send('email_register', $mail_data, function($m) use ($person) {
            $m->from('noreply@clockme.co');
            $m->subject('Welcome to Clockme');
            $m->to($person->email);
          });

          //authenticate and redirect
          $authenticate_params = [
            '_token' => $request->_token,
            'email' => $request->email,
            'password' => $request->password,
          ];

          $request_obj = new Request;
          $request_obj->email = $request->email;
          $request_obj->password = $request->password;

          if ($this->_attempt($authenticate_params)) {
            return $this->handleUserWasAuthenticated($request_obj, null);
          }

        }else{
          return redirect('/register')->with('message', 'Error occur, please contact our support team');
        }
      }else{
        return redirect('/register')->with('message', 'Error occur, please contact our support team');
      }
    }

    private function _encryptStr($str){

      $key = 'guess the key mate if you can';
      $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));
      return $encrypted;

    }

    private function _registrationValidate($request) {

      \Log::debug($request);

      return false;


    }
}
