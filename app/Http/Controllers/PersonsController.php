<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Person;
use App\Company;
use App\CompanyMap;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use URL;
use Mail;

class PersonsController extends Controller
{

  public function __construct() {
      $this->middleware('auth', ['except' => ['emailvalidation']]);
  }

	public function index(Request $request) {

		$persons = Person::getPersonWithActiveLabel();

		return view('persons', [
			'page_title' => 'Persons',
			'persons' => $persons,
		]);

	}

	public function save(Request $request) {

    //Need to validate the inforations

		$person = new Person();

		$person->name = $request->name;
		$person->email = $request->email;
		$person->mobile = $request->mobile;
		$person->password = $this->_encryptStr($request->password);
		//$person->password = $this->_customEncrypt($request->password);
		// $person->isPerson = Person::$person;
		$person->active = Person::$activeStatus;

		$person->save();

    //dd($person);
    //Save person to companyMap
    //Need to save the company id when user login
    $companyMap = new CompanyMap();
    $companyMap->companyId = CompanyMap::getCurrentCompanyId();
    $companyMap->personId = $person->personId;

    if ($request->driver == 0) {
      $companyMap->driver = 2;
    }else{
      $companyMap->driver = 1;
    }

    $companyMap->save();

		$mail_data = [
			'title' => 'Person saved successfuly.',
			'name' => $request->name,
			'email' => $request->email,
			'password' => $request->password,
		];

		Mail::send('email_welcome', $mail_data, function($m) use ($person) {

			$m->from('noreply@asyncme.com');
			$m->subject('Welcome to DeliveryMe');
			$m->to($person->email);

		});

		//return redirect()->action('PersonsController@index', ['result' => 'savesuccess']);
		return redirect()->back()->with(['result' => 'The person has been saved.', 'resultstatus' => 'success']);

	}

	public function update(Request $request) {

		$person = Person::find($request->personId);

		$person->name = $request->name;
		$person->email = $request->email;
		$person->mobile = $request->mobile;
		// $person->isPerson = Person::$person;

		if ($request->active) {

			$person->active = Person::$activeStatus;

		} else {

			$person->active = Person::$inactiveStatus;

		}

		$person->save();

    $companyMap = CompanyMap::where('companyId',CompanyMap::getCurrentCompanyId())
                              ->where('personId',$person->personId)
                              ->first();

    if ($request->driver) {
      $companyMap->driver = 1;
    }else{
      $companyMap->driver = 2;
    }

    $companyMap->save();

		//return redirect()->action('PersonsController@index', ['result' => 'updatesuccess']);
		return redirect()->back()->with(['result' => 'The change has been saved.', 'resultstatus' => 'success']);

	}

	public function delete(Request $request) {

		$person = Person::find($request->personId);
		$person->active = 0;
		$person->save();

		//return redirect()->action('PersonsController@index', ['result' => 'deletesuccess']);
		return redirect()->back()->with(['result' => 'The person has been deleted.', 'resultstatus' => 'success']);

	}

	private function _customEncrypt ($str){

		$key = 'guess the key mate if you can';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));

		return $encrypted;

	}

	//Workaround wrapper for setting session flash, as the default session flash does not work
	private function _setSessionFlash($flag) {

		if ($flag == 'savesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The person has been saved.',
			]);

		} else if ($flag == 'updatesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The change has been saved.',
			]);

		} else if ($flag == 'deletesuccess') {

			session([
				'textStatus' => 'success',
				'message' => 'The person has been deleted.',
			]);

		} else if ($flag == 'savefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The person could not be added. Please try again.'
			]);

		} else if ($flag == 'updatefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The change could not be saved. Please try again.'
			]);

		} else if ($flag == 'deletefailure') {

			session([
				'textStatus' => 'danger',
				'message' => 'The person could not be deleted. Please try again.'
			]);

		}

	}

  public function register(Request $request) {
    dd($request);
    //Need to validate the inforations

		$person = new Person();

		$person->name = $request->name;
		$person->email = $request->email;
		$person->mobile = $request->mobile;
		$person->password = bcrypt($request->password);
		//$person->password = $this->_customEncrypt($request->password);
		// $person->isPerson = Person::$person;
		$person->active = Person::$activeStatus;

		$person->save();

    if ($person->personId) {

      //Save company
      $company = new Company();
      $company->companyName = $request->company;
      $company->adminId = $person->personId;
      $company->timezone = "Australia/Melbourne";

      $company->save;

      if ($company->companyId) {

        //Save person to companyMap
        //Need to save the company id when user login
        $companyMap = new CompanyMap();
        $companyMap->companyId = $company->companyId;
        $companyMap->personId = $person->personId;
        $companyMap->driver = 1;

        $companyMap->save();

        $mail_data = [
    			'title' => 'Person saved successfuly.',
    			'name' => $request->name,
    			'email' => $request->email,
    			'password' => $request->password,
    		];

        return redirect()->action('login', ['result' => 'savesuccess']);

        Mail::send('email_register', $mail_data, function($m) use ($person) {

    			$m->from('noreply@asyncme.com');
    			$m->subject('Welcome to DeliveryMe');
    			$m->to($person->email);

    		});

      }else{

        return redirect()->action('register', ['result' => 'failed']);

      }

    }else{

      return redirect()->action('register', ['result' => 'failed']);

    }

	}

	public function emailvalidation(Request $request) {

		$result = [];

		if ($request->email) {

			if ($request->exception_id) {

				$existing_emails = Person::where('email', $request->email)
					->whereNotIn('personId', [$request->exception_id])
					->first();

			} else {

				$existing_emails = Person::where('email', $request->email)->first();

			}

			if (!$existing_emails) {

				$result = [
					'valid' => true
				];

			} else {

				$result = [
					'valid' => false,
					'message' => 'The email already exists. Please use a different email address.',
				];

			}

		}

		return response()->json($result);

	}

  private function _encryptStr($str){

    $key = 'guess the key mate if you can';
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));
    return $encrypted;

  }

  public function resendEmail(){

    $person = Person::find($_POST['personId']);

    $mail_data = [
      'title' => 'Welcome to Deliveryme.',
      'name' => $person->name,
      'email' => $person->email,
      'password' =>  Person::_decryptStr($person->password),
    ];

    Mail::send('email_welcome', $mail_data, function($m) use ($person) {

			$m->from('noreply@asyncme.com');
			$m->subject('Welcome to DeliveryMe');
			$m->to($person->email);

		});

    return redirect('/persons')->with(['result' => 'Email sent to: '.$person->email .' successfuly.', 'resultstatus' => 'success']);

  }

  public function followupEmail(Request $request){

    // 4 - phsgjc@gmail.com
    $person = Person::find($request->personId);

    $mail_data = [
      'title' => 'Paul from DeliveryMe',
      'name' => $person->name,
      'email' => $person->email,
    ];

    Mail::send('email_followup_1', $mail_data, function($m) use ($person) {

      $m->from('paul@asyncme.com');
      $m->subject('Paul from DeliveryMe');
      $m->to($person->email);

    });

  }

}
