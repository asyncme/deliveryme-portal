<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompanySetting;
use App\Currency;
use App\CompanyMap;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Person
use Log;

class SettingsController extends Controller
{

  public function __construct() {
      $this->middleware('auth');
  }

	public function index(Request $request) {

		$setting = CompanySetting::find(CompanyMap::getCurrentCompanyId());

		return view('settings', [
			'page_title' => 'Settings',
			'setting' => $setting,
		]);

	}

	public function save(Request $request) {

		$setting = CompanySetting::where('companyId',CompanyMap::getCurrentCompanyId())->first();
		//$setting = Setting::findOrFail(CompanyMap::getCurrentCompanyId());

		$setting->currencyId = $request->input('currency');
		$setting->normalStartTime = $request->input('normal-time-from');
		$setting->normalEndTime = $request->input('normal-time-to');
		$setting->afterHourRate = $request->input('afterhour-rate');
		$setting->saturdayRate = $request->input('saturday-rate');
		$setting->sundayRate = $request->input('sunday-rate');
		$setting->maxLength = $request->input('maxworkhours');
		$setting->breakThreshold = $this->_convertMinToSecond($request->input('break-threshold'));
		$setting->breakLength = $this->_convertMinToSecond($request->input('break-length'));
		//$setting->person->email = $request->input('email');

		$setting->save();
		$setting->person->save();

		//return redirect()->back()->with('result', 'The setting has been updated');
		return redirect()->back()->with(['result' => 'The setting has been updated.', 'resultstatus' => 'success']);

	}

	private function _convertSecondToMin($second) {

		//return (int)$second / 60;
		return (int)number_format($second / 60, 0, '', '');

	}

	private function _convertMinToSecond($second) {

		//return (int)$second * 60;
		return (int)number_format($second * 60, 0, '', '');

	}


}
