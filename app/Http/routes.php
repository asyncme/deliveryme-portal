<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {

  Route::auth();
  Route::get('/', 'Auth\AuthController@getLogin');

  // Route::get('/', 'RouteController@index');
  Route::get('/routes', 'RouteController@index');
  Route::post('/routes/save', 'RouteController@save');
  Route::post('/routes/update', 'RouteController@update');
  Route::post('/routes/delete', 'RouteController@delete');

  Route::get('/customers', 'CustomersController@index');
  Route::post('/customers/save', 'CustomersController@save');
  Route::post('/customers/update', 'CustomersController@update');
  Route::post('/customers/delete', 'CustomersController@delete');

  Route::get('/persons', 'PersonsController@index');
	Route::post('/persons/save', 'PersonsController@save');
	Route::post('/persons/emailvalidation', 'PersonsController@emailvalidation');
	Route::post('/persons/update', 'PersonsController@update');
	Route::post('/persons/delete', 'PersonsController@delete');
  Route::post('/persons/resendEmail', 'PersonsController@resendEmail');

  Route::get('/settings', 'SettingsController@index');
  Route::post('/settings', 'SettingsController@save');

  //Followup email
  // Route::get('/followup/{id}', 'PersonsController@followupEmail');

});
