<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

	protected $table = 'company';
	protected $primaryKey = 'companyId';

	public static $adminUser = 1;
	public static $normalUser = 2;

	public $timestamps = false;

	public static function getCompany() {

		return Company::where('active','=','1')->get();

	}

	public static function getCompanyById($companyId) {

		return Company::where('active','=','1')
			->where('companyId','=',$companyId)
			->first();

	}

}
