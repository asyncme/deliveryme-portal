module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        files: [{
          cwd: 'js',
          src: 'js/*.js',
          dest: 'build/js',
          ext: 'js'
        }]
      }
    },
    sass: {
      dist: {
        files: [{
          //cwd: 'scss',
          src: 'scss/pages.scss',
          dest: 'css/style.css'
          //ext: 'css'
        }]
      }
    },
    watch: {
      scripts: {
        files: ['scss/pages.scss'],
        tasks: ['sass']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  //grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['sass', 'watch']);

};
