$(function() {

	$('.validation-form').on('submit', function(e) {

		e.preventDefault();

		var form = $(e.target);
		var email = form.find('input[type="email"]').val();
		var token = form.find('input[name="_token"]').val();
		var id = form.find('input[name="id"]');

		var send_data = {
			email: email,
			_token: token
		};

		if (id.length > 0) {

			send_data['exception_id'] = id.val();

		}

		var message_target = form.find('.validation-error').empty();

		var options = {
			url: '/persons/emailvalidation',
			type: 'post',
			headers: {
				'X-CSRF-TOKEN': $('#csrf-token').attr('content')
			},
			data: send_data,
			dataType: 'json',
			beforeSend: function() {

				message_target.addClass('hidden');

			},
			success: function(result) {

				console.log(result);

				if (result.valid) {

					form.off('submit').submit();

				} else {

					message_target.removeClass('hidden');
					message_target.html(result.message + '<br />');

				}

			}
		}

		$.ajax(options);

	});
	
});
