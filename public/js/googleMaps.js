
var map;
var markers = [];
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
// var labels = '1234567891011';

function initMap(name,ready) {
  map = new google.maps.Map(document.getElementById(name), {
    zoom: 8,
    center: new google.maps.LatLng(parseFloat(0.0),parseFloat(0.0)),
  });

  google.maps.event.addListenerOnce(map, 'idle', function(){
    ready();
  });

  // Clean markers array
  markers = [];

  // center: {lat: 0.0, lng: 0.0}
  console.log("Map initialized: " + name);
}

function addMarker(id,position){

  if(map){

    showMakers(null);

    for (var i = 0; i < arrayCustomers.length; i++) {
      var customer = arrayCustomers[i];
      if (customer.customerId == id) {

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(parseFloat(customer.lat),parseFloat(customer.lng)),
          title: customer.name,
          customerId: customer.customerId
        });

        var infowindow = new google.maps.InfoWindow({
          content: customer.name
        });

        marker.addListener('click', function(){
          infowindow.open(map,marker);
        });

        markers.splice(position,0,marker);
        break;
      }
    }

    showMakers(map)
  }

}

function updateMarker(id, position){

  for (var i = 0; i < markers.length; i++) {
    var marker = markers[i];
    if (marker.customerId == id) {
      console.log("update: " + marker.title);
      markers.splice(i,1);
    }
  }

  //Add marker again with new position
  addMarker(id,position);

}

function removeMarker(id){

  // remove all markers
  showMakers(null);

  for (var i = 0; i < markers.length; i++) {
    var marker = markers[i];
    if (marker.customerId == id) {
      console.log("remove: " + marker.title);
      markers.splice(marker,1);
    }
  }

  //re add all markers
  showMakers(map);

}

function showMakers(map){

  for (var i = 0; i < markers.length; i++) {
    var marker = markers[i];
    marker.setLabel(labels[i % labels.length]);
    marker.setMap(map)
  }

  setTimeout(centerMap, 500);

}

function centerMap(){

  var bounds = markers.reduce(function(bounds, marker) {
    return bounds.extend(marker.getPosition());
  }, new google.maps.LatLngBounds());

  map.setCenter(bounds.getCenter());
  map.fitBounds(bounds);

}

$('#modal-add').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, "resize");
});
