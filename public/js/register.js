$(function() {

	$('.validation-form').on('submit', function(e) {

		e.preventDefault();

		var form = $(e.target);

		var email = form.find('input[type="email"]').val();
		var token = form.find('input[name="_token"]').val();
		var password = form.find('input[name="password"]').val();
		var password_confirmation = form.find('input[name="password_confirmation"]').val();

		if (password != password_confirmation) {

			$('#password-error').remove();
			
			var message_target = $('input[name="password_confirmation"]');

			var message = $('<span id="password-error">').addClass('help-block').append(
				$('<strong>').addClass('text-danger').text('Passwords do not match. Please enter the same password for confirmation.')
			);

			message_target.after(message);

		} else {

			$('#password-error').remove();

			var send_data = {
				email: email,
				_token: token
			};

			//var message_target = form.find('.validation-error').empty();
			var message_target = $('input[name="email"]');

			var options = {
				url: '/persons/emailvalidation',
				type: 'post',
				headers: {
					'X-CSRF-TOKEN': $('#csrf-token').attr('content')
				},
				data: send_data,
				dataType: 'json',
				beforeSend: function() {

					$('#email-error').remove();

				},
				success: function(result) {

					console.log(result);

					if (result.valid) {

						form.off('submit').submit();

					} else {

						var message = $('<span id="email-error">').addClass('help-block').append(
							$('<strong>').addClass('text-danger').text(result.message)
						);

						message_target.after(message);
						//message_target.removeClass('hidden');
						//message_target.html(result.message + '<br />');

					}

				}
			}

			$.ajax(options);

		}

	});

});