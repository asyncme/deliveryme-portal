//datepicker
$('.datepicker-custom').datepicker({
  format: 'dd-mm-yyyy'
}).on('changeDate', function(e) {
  $(e.target).datepicker('hide');
});

//timepicker
$('.timepicker-custom').timepicker();

var deliveries;
var customers;
var deliveriesEdit;
var customersEdit;

function populateData(){

  setTimeout(function () {
    initMap('map',function(){
      buildDeliveries();
      buildCustomers(arrayCustomers);
    });
  }, 500);

}

function rePopulateData(routeId){

  var route = null;
  var routeMap = null;
  for (var r in routes) {
    if (routes.hasOwnProperty(r)) {
      if (routes[r].route.routeId == routeId) {
        route = routes[r].route;
        routeMap = routes[r].routeMap;
      }
    }
  }

  // remove from arrayCustomer the ones already added
  var copyCustomers = arrayCustomers.slice(0);

  for (var rm in routeMap) {

    if (routeMap.hasOwnProperty(rm)) {
      var rmi = routeMap[rm];

      for (var c in copyCustomers) {

        if (copyCustomers.hasOwnProperty(c)) {
          ci = copyCustomers[c];
          if(rmi.customerId == ci.customerId){
            var index = copyCustomers.indexOf(ci);
            if (index > -1) {
              copyCustomers.splice(index,1);
              break;
            }
          }
        }

      }
    }
  }

  setTimeout(function () {

    initMap('mapEdit-'+routeId,function(){

      buildDeliveriesEdit(routeMap, routeId);
      buildCustomersEdit(copyCustomers, routeId);

      // Add to map
      var i = 0;
      for (var d in routeMap) {
        if (routeMap.hasOwnProperty(d)) {
          // populate map
          var delivery = routeMap[d];
          addMarker(delivery.customerId,i);
          i++;
        }
      }

    });

  }, 500);

}

function validateAndSave(){

  var routeName = document.getElementById("form-add-route-name");
  var startDate = document.getElementById("form-add-route-start-date");
  var startTime = document.getElementById("form-add-route-start-time");
  var drivers = document.getElementById("form-add-personId");
  var listD = deliveries.toArray();
  var driver = drivers.options[drivers.selectedIndex];
  var notes = getRouteNotes(listD);

  if(routeName.value.length == 0){
    return {"success":false,"message":"Error: Route name missing"};
  }else if(startDate.value.length == 0){
    console.log('No start date set');
    return {"success":false,"message":"Error: Start Date missing"};
  }else if(startTime.value.length == 0){
    console.log('No start time set');
    return {"success":false,"message":"Error: Start Time missing"};
  }else if (listD.length == 0) {
    console.log('Empty list of customers');
    return {"success":false,"message":"Error: list of customers empty"};
  }else if (driver.value == 0) {
    console.log('Not driver selected');
    return {"success":false,"message":"Error: No driver selected"};
  }else{

    var options = {
			url: '/routes/save',
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('#csrf-token').attr('content')
			},
			data: {
				_token:$('#csrf-token').attr('content'),
        name:routeName.value,
        startDate:startDate.value,
        startTime:startTime.value,
        list:listD,
        notes: JSON.stringify(notes),
        driver:driver.value
			},
			success: function(result) {
        $('#modal-add').modal( 'hide' );
        window.location.href = 'routes';
        return {"success":true,"message":"Proceed to create route"};
			},
      error: function(error){
        return {"success":false,"message":"Error saving route"};
      }
		}

		$.ajax(options);

  }

}

function validateAndSaveUpdate(routeId){

  var routeName = document.getElementById("form-edit-route-name-"+routeId);
  var startDate = document.getElementById("form-edit-route-start-date-"+routeId);
  var startTime = document.getElementById("form-edit-route-start-time-"+routeId);
  var drivers = document.getElementById("form-edit-personId-"+routeId);
  var listD = deliveriesEdit.toArray();
  var driver = drivers.options[drivers.selectedIndex];
  var notes = getRouteNotes(listD);

  if(routeName.value.length == 0){
    console.log('No name set');
    return {"success":false,"message":"Error: Route name missing"};
  }else if(startDate.value.length == 0){
    console.log('No start date set');
    return {"success":false,"message":"Error: Start Date missing"};
  }else if(startTime.value.length == 0){
    console.log('No start time set');
    return {"success":false,"message":"Error: Start Time missing"};
  }else if (listD.length == 0) {
    console.log('Empty list of customers');
    return {"success":false,"message":"Error: list of customers empty"};
  }else if (driver.value == 0) {
    console.log('Not driver selected');
    return {"success":false,"message":"Error: No driver selected"};
  }else{

    var options = {
			url: '/routes/update',
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('#csrf-token').attr('content')
			},
			data: {
				_token:$('#csrf-token').attr('content'),
        name:routeName.value,
        startDate:startDate.value,
        startTime:startTime.value,
        routeId:routeId,
        list:listD,
        notes: JSON.stringify(notes),
        driver:driver.value
			},
			success: function(result) {
        console.log(result);
        $('#modal-edit-'+routeId).modal( 'hide' );
        window.location.href = 'routes';
			},
      error: function(error){
        console.log(error);
        $('#modal-edit-'+routeId).modal( 'hide' );
      }
		}

		$.ajax(options);

  }

}


////////////////////////////////////////////////////////////////////////////////
/////////////////////////////    ADDING   //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function buildCustomers(listCustomer){

  // Start fresh
  $('#Customers').empty();

  customers = Sortable.create(Customers, {
    group: {
      name: 'Customers',
      put: ['Deliveries'],
      sort: true
    },
    animation: 100,
    onAdd: function(evt){
      evt.item.className = "customUl";
      var customer = getCustomerById(evt.item.getAttribute("data-id"));
      evt.item.innerHTML = customer.name;
      removeMarker(evt.item.getAttribute("data-id"));
    }
  });

  if(listCustomer != undefined){

    for (var c in listCustomer) {
      if (listCustomer.hasOwnProperty(c)) {
        var customer = listCustomer[c];
        $("#Customers").append($("<li class='customUl' data-id="+customer.customerId+">").text(customer.name));
      }
    }

  }

}

function buildDeliveries(listDelivery){

  // start fresh
  $('#Deliveries').empty();
  var el = document.getElementById('Deliveries');
  el.className = "customUlDelivery";

  deliveries = Sortable.create(Deliveries, {
    group: {
      name: 'Deliveries',
      put: ['Customers']
    },
    animation: 100,
    onAdd: function(evt){
      evt.item.className = "customUlDelivery";
      evt.item.innerHTML += '</br>Notes: <span><input type="text" id="note-id-'+evt.item.getAttribute("data-id")+'"></input></span>';
      addMarker(evt.item.getAttribute("data-id"),evt.newIndex);
    },
    onUpdate: function(evt){
      updateMarker(evt.item.getAttribute("data-id"),evt.newIndex);
    }
  });
}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////////    EDITING    ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function buildCustomersEdit(listCustomer, routeId){

  // Start fresh
  $('#CustomersEdit-'+routeId).empty();

  var el = document.getElementById('CustomersEdit-'+routeId);
  customersEdit = Sortable.create(el, {
    group: {
      name: 'CustomersEdit-'+routeId,
      put: ['DeliveriesEdit-'+routeId],
      sort: true
    },
    onAdd: function(evt){
      evt.item.className = "customUl";
      var customer = getCustomerById(evt.item.getAttribute("data-id"));
      evt.item.innerHTML = customer.name;
      removeMarker(evt.item.getAttribute("data-id"));
    },
    animation: 100
  });

  if(listCustomer != undefined){
    for (var c in listCustomer) {
      if (listCustomer.hasOwnProperty(c)) {
        var customer = listCustomer[c];
        $("#CustomersEdit-"+routeId).append($("<li class='customUl' data-id="+customer.customerId+">").text(customer.name));
      }
    }

  }

}

function buildDeliveriesEdit(listDelivery, routeId){

  // start fresh
  $('#DeliveriesEdit-'+routeId).empty();

  var el = document.getElementById('DeliveriesEdit-'+routeId);
  el.className = "customUlDelivery";

  deliveriesEdit = Sortable.create(el, {
    group: {
      name: 'DeliveriesEdit-'+routeId,
      put: ['CustomersEdit-'+routeId]
    },
    onAdd: function(evt){
      evt.item.className = "customUlDelivery";
      evt.item.innerHTML += '</br>Notes: <span><input type="text" id="note-id-'+evt.item.getAttribute("data-id")+'" value=""></input></span>';
      addMarker(evt.item.getAttribute("data-id"),evt.newIndex);
    },
    onUpdate: function(evt){
      updateMarker(evt.item.getAttribute("data-id"),evt.newIndex);
    },
    animation: 100,
  });

  if(listDelivery != undefined){
    for (var d in listDelivery) {
      if (listDelivery.hasOwnProperty(d)) {
        var delivery = listDelivery[d];
        var del = document.getElementById('DeliveriesEdit-'+routeId);
        if (delivery.note != undefined && delivery.note.length > 0) {
          del.innerHTML += "<li class='customUlDelivery' data-id="+delivery.customerId+">" + delivery.name + "</br>Notes: <span><input type='text' id='note-id-"+delivery.customerId+"' value='"+delivery.note+"'></input></span></li>";
        }else{
          del.innerHTML += "<li class='customUlDelivery' data-id="+delivery.customerId+">" + delivery.name + "</br>Notes: <span><input type='text' id='note-id-"+delivery.customerId+"' value=''></input></span></li>";
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
///////////////////////////    FUNCTIONS    ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
function getRouteNotes(listD){

  var notes = [];
  for (var d in listD) {
    if (listD.hasOwnProperty(d)) {
      // populate map
      var delivery = listD[d];
      var note = document.getElementById("note-id-"+delivery).value;
      if(note.length != 0){
        notes.push(
          {
            customerId: delivery,
            note: note
          });
      }
    }
  }

  return notes;

}

function getCustomerById(customerId){
  for (var c in arrayCustomers) {
    if (arrayCustomers.hasOwnProperty(c)) {
      var customer = arrayCustomers[c];
      if (customer.customerId == customerId) {
        return customer;
      }
    }
  }
}
