$(document).ready(function() {

	$('#calendar-timesheet').fullCalendar({
				schedulerLicenseKey: '0073896728-fcs-1456106971',
				defaultDate: '2016-01-01',
				editable: true,
				eventLimit: true, // allow "more" link when too many events,
				header: {
					left: 'today prev,next',
					center: 'title',
					right: 'timelineDay,timelineThreeDays,agendaWeek,month'
				},
				resources: [
					{ id: 'a', building: '460 Bryant', title: 'Auditorium A', occupancy: 40 },
					{ id: 'b', building: '460 Bryant', title: 'Auditorium B', occupancy: 40, eventColor: 'green' },
					{ id: 'c', building: '460 Bryant', title: 'Auditorium C', occupancy: 40, eventColor: 'orange' },
					{ id: 'd', building: '460 Bryant', title: 'Auditorium D', occupancy: 40, children: [
						{ id: 'd1', title: 'Room D1', occupancy: 10 },
						{ id: 'd2', title: 'Room D2', occupancy: 10 }
					] },
					{ id: 'e', building: '460 Bryant', title: 'Auditorium E', occupancy: 40 },
					{ id: 'f', building: '460 Bryant', title: 'Auditorium F', occupancy: 40, eventColor: 'red' },
					{ id: 'g', building: '564 Pacific', title: 'Auditorium G', occupancy: 40 },
					{ id: 'h', building: '564 Pacific', title: 'Auditorium H', occupancy: 40 },
					{ id: 'i', building: '564 Pacific', title: 'Auditorium I', occupancy: 40 },
					{ id: 'j', building: '564 Pacific', title: 'Auditorium J', occupancy: 40 },
					{ id: 'k', building: '564 Pacific', title: 'Auditorium K', occupancy: 40 },
					{ id: 'l', building: '564 Pacific', title: 'Auditorium L', occupancy: 40 },
					{ id: 'm', building: '564 Pacific', title: 'Auditorium M', occupancy: 40 },
					{ id: 'n', building: '564 Pacific', title: 'Auditorium N', occupancy: 40 },
					{ id: 'o', building: '564 Pacific', title: 'Auditorium O', occupancy: 40 },
					{ id: 'p', building: '564 Pacific', title: 'Auditorium P', occupancy: 40 },
					{ id: 'q', building: '564 Pacific', title: 'Auditorium Q', occupancy: 40 },
					{ id: 'r', building: '564 Pacific', title: 'Auditorium R', occupancy: 40 },
					{ id: 's', building: '564 Pacific', title: 'Auditorium S', occupancy: 40 },
					{ id: 't', building: '564 Pacific', title: 'Auditorium T', occupancy: 40 },
					{ id: 'u', building: '564 Pacific', title: 'Auditorium U', occupancy: 40 },
					{ id: 'v', building: '564 Pacific', title: 'Auditorium V', occupancy: 40 },
					{ id: 'w', building: '564 Pacific', title: 'Auditorium W', occupancy: 40 },
					{ id: 'x', building: '564 Pacific', title: 'Auditorium X', occupancy: 40 },
					{ id: 'y', building: '564 Pacific', title: 'Auditorium Y', occupancy: 40 },
					{ id: 'z', building: '564 Pacific', title: 'Auditorium Z', occupancy: 40 }
				],
				events: [
					{
						id: 2,
						title: 'Shuhei',
						start: '2016-01-01T09:00:00',
						end: '2016-01-05T18:00:00',
						color: '#1A5276',
						resourceId: 'a'
					},
					{
						id: 3,
						title: 'Paulo',
						start: '2016-01-06T09:00:00',
						end: '2016-01-10T18:00:00',
						color: '#1A5276',
						resourceId: 'a'
					},
					{
						id: 5,
						title: 'Shuhei',
						start: '2016-01-05T09:00:00',
						end: '2016-01-10T18:00:00',
						color: '#EB5F31',
						resourceId: 'e'

					},
					{
						id: 6,
						title: 'Paulo',
						start: '2016-01-10T09:00:00',
						end: '2016-01-15T18:00:00',
						color: '#EB5F31',
						resourceId: 'e'

					}
				],
				eventRender: function (event, element) {
				   element.find('.fc-title').html(event.title);
				}
			})

});
