<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{ $page_title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta content="robots" name="noindex, nofollow" />
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN Vendor CSS-->
    <link href="{!! URL::asset('assets/plugins/pace/pace-theme-flash.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::asset('assets/plugins/boostrapv3/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::asset('assets/plugins/font-awesome/css/font-awesome.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') !!}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{!! URL::asset('assets/plugins/bootstrap-select2/select2.css') !!}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{!! URL::asset('assets/plugins/switchery/css/switchery.min.css') !!}" rel="stylesheet" type="text/css" media="screen" />
    <!-- BEGIN Pages CSS-->

    <link href="{!! URL::asset('pages/css/pages-icons.css') !!}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{!! URL::asset('pages/css/pages.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::asset('pages/css/style.css') !!}" rel="stylesheet" type="text/css" />

    @yield('styles')

    <!--[if lte IE 9]>
      <link href="{!! URL::asset('pages/css/ie9.css') !!}" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="">

    <!-- START PAGE-CONTAINER -->
    <div class="container-fluid container-fixed-lg">
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content nonav">

            @yield('content')

        </div>
        <!-- END PAGE CONTENT -->
        <!-- START FOOTER -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2015</span>
              <span class="font-montserrat">Deliveryme</span>.
              <span class="hint-text">All rights reserved.</span>
              <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a>
                        </span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END FOOTER -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- Modal - add -->
    <div id="modal-add" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Resource</h4>
          </div>
          <div class="modal-body">

            <form class="container-fluid row">
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-add-name">Name</label>
                <input type="text" class="form-control" id="form-add-name" placeholder="Name">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-add-email">Email</label>
                <input type="email" class="form-control" id="form-add-email" placeholder="Email">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-add-mobile">Mobile</label>
                <input type="text" class="form-control" id="form-add-mobile" placeholder="Mobile">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label>&nbsp;</label>
                <div class="checkbox check-success">
                  <input type="checkbox" checked="checked" value="1" id="form-add-isperson">
                  <label for="form-isperson">This is a staff member</label>
                </div>
              </div>
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success">Save</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal - edit -->
    <div id="modal-edit" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Resource</h4>
          </div>
          <div class="modal-body">

            <form class="container-fluid row">
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-edit-name">Name</label>
                <input type="text" class="form-control" id="form-edit-name" placeholder="Name" value="FirstName LastName">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-edit-email">Email</label>
                <input type="email" class="form-control" id="form-edit-email" placeholder="Email" value="banana@sample.com">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label for="form-edit-mobile">Mobile</label>
                <input type="text" class="form-control" id="form-edit-mobile" placeholder="Mobile" value="00433332222">
              </div>
              <div class="form-group col-md-6 col-lg-6">
                <label>&nbsp;</label>
                <div class="checkbox check-success">
                  <input type="checkbox" checked="checked" value="1" id="form-edit-isperson">
                  <label for="form-edit-isperson">This is a staff member</label>
                </div>
              </div>
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success">Save</button>
          </div>
        </div>
      </div>
    </div>

    <script src="{!! URL::asset('assets/plugins/pace/pace.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/jquery/jquery-1.11.1.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/modernizr.custom.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-ui/jquery-ui.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/boostrapv3/js/bootstrap.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/jquery/jquery-easy.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-bez/jquery.bez.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::asset('assets/plugins/imagesloaded/imagesloaded.pkgd.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-actual/jquery.actual.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') !!}"></script>

    <script src="{!! URL::asset('pages/js/pages.js') !!}" type="text/javascript"></script>

    <script src="{!! URL::asset('js/script.js') !!}" type="text/javascript"></script>

    @yield('scripts')

  </body>
</html>
