@extends('master')

@section('styles')

  <link media="screen" type="text/css" rel="stylesheet" href="{!! URL::asset('bootstrap-colorpicker/css/bootstrap-colorpicker.css') !!}">
  <link media="screen" type="text/css" rel="stylesheet" href="{!! URL::asset('assets/css/sortable.css') !!}">

@stop

@section('content')

      <div class="jumbotron subheader">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb pull-left">
              <li>
                <p>Route Schedule&nbsp;<i class="fa fa-map-marker"></i></p>
              </li>
            </ul>
            <div class="pull-right subheader-button">
              <a class="btn btn-primary" data-toggle="modal" onclick="populateData();" data-target="#modal-add">Create Route</a>
            </div>
            <!-- END BREADCRUMB -->
          </div>
        </div>
      </div>

      @if (session('result'))

        <div class="alert alert-{{ session('resultstatus') }} hide-later">
          {{ session('result') }}
        </div>

      @endif


      <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg main-body">

        <table class="table table-striped">
          <thead>
            <tr>
              <th width="">Name</th>
              <th width="">Time</th>
              <th width="">Driver</th>
              <th width="">Quantity</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($routes as $route)

              <tr>
                <td>{!! $route['route']->name !!}</td>
                <td>{{ date('d-m-Y H:i:s', $route['route']->time) }}</td>
                <td>{{ $route['route']->personName }}</td>
                <td>
                  <a class="btn btn-primary" data-toggle="modal" onclick="rePopulateData({!! $route['route']->routeId !!});" data-target="#modal-edit-{!! $route['route']->routeId !!}">Edit</a>
                  <a class="btn btn-default" data-toggle="modal" data-target="#modal-delete-{!! $route['route']->routeId !!}">Delete</a>
                </td>
              </tr>

            @endforeach

          </tbody>
        </table>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

    <!-- Modal - add -->
    <div id="modal-add" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <form class="ajax-form" method="post" action="/routes/save">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Route</h4>
            </div>
            <div class="modal-body">

              <div class="container-fluid row">
                <div class="form-group col-md-12 col-lg-12">
                  <label for="form-add-route-name">Name</label>
                  <input name="name" class="form-control submit-data" id="form-add-route-name" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label>Start Date</label>
                  <div class="input-group">
                    <input type="text" name="startdate" id="form-add-route-start-date" class="form-control datepicker-custom" placeholder="dd-mm-yyyy" readonly required>
                  </div>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label>Start Time</label>
                  <div class="input-group">
                    <input type="text" name="starttime" id="form-add-route-start-time" class="form-control timepicker-custom" required>
                  </div>
                </div>
                <!-- START - Sortable -->
                <div class="form-group col-md-6 col-lg-6" id="form-deliveries">
                  <label>Deliveries</label>
                  <ul id="Deliveries" class="customUl" style="min-height:100px;">
                  </ul>
                </div>
                <div class="form-group col-md-6 col-lg-6" id="form-customers">
                  <label>Customers</label>
                  <ul id="Customers" class="customUl" style="min-height:100px;">
                  </ul>
                </div>
                <!-- END - Sortable -->
                <div class="form-group col-md-12 col-lg-12">
                <label for="form-add-personId">Driver</label>
                  <select name="personId" class="form-control" id="form-add-personId" required>
                    <option value="0">Select</option>
                  @foreach ($persons as $person)

                    <option value="{{ $person->personId }}">{{ $person->name }} - {{ $person->email }}</option>

                  @endforeach

                  </select>
                </div>

                <div class="col-md-12 col-lg-12" style="margin-top:10px;">
                  <p><strong>Tip: </strong>Drag from Customers to Deliveries</p>
                </div>

                <div id="map" class="map"></div>

              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="alert(validateAndSave().message);">Save</button>
              {{ csrf_field() }}
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal - edit -->
    @foreach ($routes as $route)

    <?php
      //Convert time + timezone to carbon date
      $date = \Carbon\Carbon::createFromTimestamp($route['route']->time,$route['route']->timezone);
    ?>

    <div id="modal-edit-{!! $route['route']->routeId !!}" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <form method="post" action="/routes/update">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit Route</h4>
            </div>
            <div class="modal-body">

              <div class="container-fluid row">
                <div class="form-group col-md-12 col-lg-12">
                  <label for="form-edit-route-name">Name</label>
                  <input name="name" class="form-control submit-data" id="form-edit-route-name-{{ $route['route']->routeId}}" value="{!! $route['route']->name !!}" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label>Start Date</label>
                  <div class="input-group">
                    <input type="text" name="startdate" id="form-edit-route-start-date-{{ $route['route']->routeId}}" class="form-control datepicker-custom" placeholder="dd-mm-yyyy" value="{!! $date->format('d-m-Y') !!}" required>
                  </div>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label>Start Time</label>
                  <div class="input-group">
                    <input type="text" name="starttime" id="form-edit-route-start-time-{{ $route['route']->routeId}}" class="form-control timepicker-custom" value="{!! $date->format('h:i A') !!}" required>
                  </div>
                </div>
                <!-- START - Sortable -->
                <div class="form-group col-md-6 col-lg-6" id="form-deliveries-edit">
                  <label>Deliveries</label>
                  <ul id="DeliveriesEdit-{{$route['route']->routeId}}" class="customUl" style="min-height:100px;">
                  </ul>
                </div>
                <div class="form-group col-md-6 col-lg-6" id="form-customers-edit">
                  <label>Customers</label>
                  <ul id="CustomersEdit-{{$route['route']->routeId}}" class="customUl" style="min-height:100px;">
                  </ul>
                </div>
                <!-- END - Sortable -->
                <div class="form-group col-md-12 col-lg-12">
                <label for="form-edit-personId">Driver</label>
                  <select name="personId" class="form-control" id="form-edit-personId-{{ $route['route']->routeId}}" value="{!! $route['route']->personName !!}" required>

                  @foreach ($persons as $person)

                    @if($person->personId == $route['route']->personId)
                    <option value="{{ $person->personId }}" selected="selected">{{ $person->name }} - {{ $person->email }}</option>
                    @else
                    <option value="{{ $person->personId }}">{{ $person->name }} - {{ $person->email }}</option>
                    @endif

                  @endforeach

                  </select>
                </div>

                <div class="col-md-12 col-lg-12" style="margin-top:10px;">
                  <p><strong>Tip: </strong>Drag from Customers to Deliveries</p>
                </div>

                <div id="mapEdit-{{ $route['route']->routeId}}" class="map"></div>

              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="alert(validateAndSaveUpdate({{ $route['route']->routeId }}).message);">Save</button>
              <input type="hidden" name="routeId" value="{!! $route['route']->routeId !!}">
              <input type="hidden" name="data-note" value="{!! $route['route']->note !!}">
              {{ csrf_field() }}
            </div>

          </form>

        </div>
      </div>
    </div>
    @endforeach

    <!-- Modal - delete -->
    @foreach ($routes as $route)

      <div id="modal-delete-{!! $route['route']->routeId !!}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">

            <form method="post" action="/routes/delete">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Route</h4>
              </div>
              <div class="modal-body">

                <p class="modal-delete-message">Are you sure you want to delete {!! $route['route']->name !!}?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Delete">
                <input type="hidden" name="routeId" value="{!! $route['route']->routeId !!}">
                {{ csrf_field() }}
              </div>

            </form>

          </div>
        </div>
      </div>

    @endforeach

@stop

@section('scripts')

  <script type="text/javascript" src="{!! URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
  <script type="text/javascript" src="{!! URL::asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js') !!}"></script>
  <script src="{!! URL::asset('js/Sortable.min.js') !!}"></script>
  <script src="{!! URL::asset('js/sortable.js') !!}"></script>
  <script src="{!! URL::asset('js/googleMaps.js') !!}"></script>
  <script src="{!! URL::asset('fullcalendar/lib/moment.min.js') !!}"></script>
  <script src="{!! URL::asset('fullcalendar/fullcalendar.js') !!}"></script>
  <script src="{!! URL::asset('fullcalendar/scheduler.js') !!}"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3eZuM1YSuDU11XL-im034HzyAxEKG0fs"></script>

  <script>
      var arrayCustomers = {!! json_encode($customers->toArray()) !!};
      var routes = {!! json_encode($routes) !!};
  </script>

@stop
