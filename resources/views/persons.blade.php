@extends('master')

@section('content')

      <div class="jumbotron subheader">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb pull-left">
              <li>
                <p>Persons/Drivers&nbsp;<i class="fa fa-users"></i></p>
              </li>
            </ul>
            <div class="pull-right subheader-button">
              <a class="btn btn-primary" data-toggle="modal" data-target="#modal-add">Create Person</a>
            </div>
            <!-- END BREADCRUMB -->
          </div>
        </div>
      </div>

      @if (session('result'))

        <div class="alert alert-{{ session('resultstatus') }} hide-later">
          {{ session('result') }}
        </div>

      @endif

      <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg main-body">

        <table class="table table-striped">
          <thead>
            <tr>
              <th width="20%">Name</th>
              <th width="30%">Email</th>
              <th width="10%">Driver</th>
              <th width="10%">Status</th>
              <th width="30%">Manage</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($persons as $person)

              <tr>
                <td>{!! $person->name !!}</td>
                <td>
                  <a href="{!! $person->maillink !!}">{!! $person->email !!}</a>
                </td>
                <td>
                  @if($person->driver == 1)
                    YES
                  @else
                    NO
                  @endif
                </td>
                <td class="{{ ($person->active) ? 'text-success' : 'text-warning' }}">{!! $person->activeLabel !!}</td>
                <td>

                  <a class="btn btn-primary" data-toggle="modal" data-target="#modal-edit-{!! $person->personId !!}">Edit</a>

                  <form method="post" action="/persons/resendEmail" class="inline">
                      <input type="submit" class="btn btn-success" value="Resend email">
                      <input type="hidden" name="personId" value="{!! $person->personId !!}">
                     {{ csrf_field() }}
                  </form>

                  @if (!$person->admin)

                    <a class="btn btn-default" data-toggle="modal" data-target="#modal-delete-{!! $person->personId !!}">Delete</a>


                  @endif

                </td>
              </tr>

            @endforeach

          </tbody>
        </table>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

    <!-- Modal - add -->
    <div id="modal-add" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <form class="validation-form" method="post" action="/persons/save">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Person</h4>
            </div>
            <div class="modal-body">

              <div class="container-fluid row">
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-name">Name</label>
                  <input type="text" name="name" class="form-control" id="form-add-name" placeholder="Name" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-email">Email</label>
                  <input type="email" name="email" class="form-control" id="form-add-email" placeholder="Email" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-mobile">Mobile</label>
                  <input type="tel" name="mobile" class="form-control" id="form-add-mobile" placeholder="Mobile" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-password">Password</label>
                  <input type="text" name="password" class="form-control" id="form-add-password" required>
                </div>
                <div class="form-group col-md-12 col-lg-12">
                  <div class="checkbox check-primary">
                    <input type="checkbox" name="active" id="form-add-admin-{!! $person->personId !!}" {{ ($person->admin) ? 'checked' : '' }} >
                    <label for="form-add-admin-{!! $person->personId !!}">Driver</label>
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <span class="text-danger hidden error-message validation-error"><br /></span>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Save">
              {{ csrf_field() }}
            </div>

          </form>

        </div>
      </div>
    </div>

    <!-- Modal - edit -->
    @foreach ($persons as $person)

      <div id="modal-edit-{!! $person->personId !!}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">

            <form class="person-update-form" method="post" action="/persons/update">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Person</h4>
              </div>
              <div class="modal-body">

                <div class="container-fluid row">
                  <div class="form-group col-md-6 col-lg-6">
                    <label for="form-edit-name">Name</label>
                    <input type="text" name="name" class="form-control" id="form-edit-name" placeholder="Name" value="{!! $person->name !!}" required>
                  </div>
                  <div class="form-group col-md-6 col-lg-6">
                    <label for="form-edit-email">Email</label>
                    <input type="email" name="email" class="form-control" id="form-edit-email" placeholder="Email" value="{!! $person->email !!}" required @if ($person->admin) disabled @endif; >
                  </div>
                  <div class="form-group col-md-6 col-lg-6">
                    <label for="form-edit-mobile">Mobile</label>
                    <input type="text" name="mobile" class="form-control" id="form-edit-mobile" placeholder="Mobile" value="{!! $person->mobile !!}" required>
                  </div>
                  <div class="form-group col-md-offset-6 col-lg-offset-6 col-md-6 col-lg-6">
                    <div class="checkbox check-primary">
                      <input type="checkbox" name="active" id="form-edit-active-{!! $person->personId !!}" value="active" {{ ($person->active) ? 'checked' : '' }} @if ($person->admin) { disabled } @endif;>
                      <label for="form-edit-active-{!! $person->personId !!}">Active</label>
                    </div>
                    <div class="checkbox check-primary">
                      <input type="checkbox" name="driver" id="form-edit-driver-{!! $person->personId !!}" value="driver" {{ ($person->driver) ? 'checked' : '' }}>
                      <label for="form-edit-driver-{!! $person->personId !!}">Driver</label>
                    </div>
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <span class="text-danger hidden error-message validation-error"><br /></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save">
                <input type="hidden" name="personId" value="{!! $person->personId !!}">
                {{ csrf_field() }}
              </div>

            </form>

          </div>
        </div>
      </div>

    @endforeach

    <!-- Modal - delete -->
    @foreach ($persons as $person)

      <div id="modal-delete-{!! $person->personId !!}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">

            <form method="post" action="/persons/delete">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Person</h4>
              </div>
              <div class="modal-body">

                <p class="modal-delete-message">Are you sure you want to delete {!! $person->name !!}?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Delete">
                <input type="hidden" name="personId" value="{!! $person->personId !!}">
               {{ csrf_field() }}
              </div>

            </form>

          </div>
        </div>
      </div>

    @endforeach

@stop

@section('scripts')

  <script src="{!! URL::asset('js/person.js') !!}"></script>

@stop
