<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
</head>
<title> {{ $title }}</title>
<body>

<style type="text/css">

html,
body {
  Margin: 0 !important;
  padding: 0 !important;
  height: 100% !important;
  width: 100% !important;
}

/* What it does: Stops email clients resizing small text. */
* {
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
  width: 100%;
}

/* What is does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
  margin:0 !important;
}

/* What it does: Stops Outlook from adding extra spacing to tables. */
table,
td {
  mso-table-lspace: 0pt !important;
  mso-table-rspace: 0pt !important;
}

/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
  border-spacing: 0 !important;
  border-collapse: collapse !important;
  table-layout: fixed !important;
  Margin: 0 auto !important;
}
table table table {
  table-layout: auto;
}

/* What it does: Uses a better rendering method when resizing images in IE. */
img {
  -ms-interpolation-mode:bicubic;
}

/* What it does: Overrides styles added when Yahoo's auto-senses a link. */
.yshortcuts a {
  border-bottom: none !important;
}

/* What it does: A work-around for iOS meddling in triggered links. */
.mobile-link--footer a,
a[x-apple-data-detectors] {
  color:inherit !important;
  text-decoration: underline !important;
}

</style>

<!-- Progressive Enhancements -->
<style>

/* What it does: Hover styles for buttons */
.button-td,
.button-a {
  transition: all 100ms ease-in;
}
.button-td:hover,
.button-a:hover {
  background: #555555 !important;
  border-color: #555555 !important;
}
</style>

<body width="100%" height="100%" bgcolor="#cccccc" style="Margin: 0;">
  <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#cccccc" style="border-collapse:collapse;"><tr><td valign="top">
    <center style="width: 100%;">

      <!-- Visually Hidden Preheader Text : BEGIN -->
      <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
        (Optional) This text will appear in the inbox preview, but not the email body.
      </div>
      <!-- Visually Hidden Preheader Text : END -->

      <div style="max-width: 600px;">
        <!--[if (gte mso 9)|(IE)]>
        <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
        <tr>
        <td>
        <![endif]-->

        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
          <tr>
            <td style="padding: 20px 0; text-align: center">
              <img src="http://DeliveryMe.co/assets/images/logo_inverted.png" width="200" height="50" alt="alt_text" border="0">
            </td>
          </tr>
        </table>
        <!-- Email Header : END -->

        <!-- Email Body : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="100%" style="max-width: 600px;">

          <!-- Hero Image, Flush : BEGIN -->
          <tr>
          </tr>
          <!-- Hero Image, Flush : END -->

          <!-- 1 Column Text : BEGIN -->
          <tr>
            <td>
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                  <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">

                    <h3>Hi {{ $name }}, </h3>

                    <p>My name is Paul and I wanted to get in touch to see how things are going with DeliveryMe.</p>

                    <p>I put up a video that talks about how the system works, it has a duration of 3 minutes and will help you get started.</p>

                  </td>
                </tr>

                <!-- Video from vimeo -->
                <tr align="center">
                  <td>
                    <a href="https://vimeo.com/156112815"><img border="0" alt="DeliveryMe - How it Works" src="http://Deliveryme.co/assets/images/img_video.png" width="50%" height="30%"></a>
                  </td>
                </tr>

                <tr>
                  <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                    <!-- Bottom message -->
                    <p>Please <a href="mailto:paul@asyncme.co?subject=feedback">contact me</a> if you have any question.</p>
                  </td>
                </tr>
        </tr>
      </table>
    </td>
  </tr>

</table>
<!-- Email Body : END -->

<!-- Email Footer : BEGIN -->
<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
  <tr>
    <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
      <br><br>
      Deliveryme co Pty Ltd - ® All right reserved<br><span class="mobile-link--footer">Melbourne - Australia</span><br><span class="mobile-link--footer">+64 03 9444 8342</span>
      <br><br>
    </td>
  </tr>
</table>
<!-- Email Footer : END -->

<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</div>
</center>
</td>
</tr>

</table>
