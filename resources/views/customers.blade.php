@extends('master')

@section('content')

      <div class="jumbotron subheader">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb pull-left">
              <li>
                <p>Customers&nbsp;<i class="fa fa-users"></i></p>
              </li>
            </ul>
            <div class="pull-right subheader-button">
              <a class="btn btn-primary" data-toggle="modal" data-target="#modal-add">Create Customer</a>
            </div>
            <!-- END BREADCRUMB -->
          </div>
        </div>
      </div>

      @if (session('result'))

        <div class="alert alert-{{ session('resultstatus') }} hide-later">
          {{ session('result') }}
        </div>

      @endif

      <!-- START CONTAINER FLUID -->
      <div class="container-fluid container-fixed-lg main-body">

        <table class="table table-striped">
          <thead>
            <tr>
              <th width="20%">Name</th>
              <th width="10%">Mobile</th>
              <th width="50%">Address</th>
              <th width="20%">Manage</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($customers as $customer)

              <tr>
                <td>{!! $customer->name !!}</td>
                <td>{!! $customer->mobile !!}</td>
                <td>{{ $customer->address }}</td>
                <td>
                  <a class="btn btn-primary" data-toggle="modal" data-target="#modal-edit-{!! $customer->customerId !!}">Edit</a>
                  <a class="btn btn-default" data-toggle="modal" data-target="#modal-delete-{!! $customer->customerId !!}">Delete</a>
                </td>
              </tr>

            @endforeach

          </tbody>
        </table>

        <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->

    <!-- Modal - add -->
    <div id="modal-add" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <form class="validation-form" method="post" action="/customers/save">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Customer</h4>
            </div>
            <div class="modal-body">

              <div class="container-fluid row">
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-name">Name</label>
                  <input type="text" name="name" class="form-control" id="form-add-name" placeholder="Name" required>
                </div>
                <div class="form-group col-md-6 col-lg-6">
                  <label for="form-add-mobile">Mobile</label>
                  <input type="tel" name="mobile" class="form-control" id="form-add-mobile" placeholder="Mobile" required>
                </div>
                <div class="form-group col-md-12 col-lg-12">
                  <label for="form-add-mobile">Address</label>
                  <input type="text" name="address" class="form-control" id="form-add-address" placeholder="Address" required>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <span class="text-danger hidden error-message validation-error"><br /></span>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Save">
              {{ csrf_field() }}
            </div>

          </form>

        </div>
      </div>
    </div>

    <!-- Modal - edit -->
    @foreach ($customers as $customer)

      <div id="modal-edit-{!! $customer->customerId !!}" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <form class="validation-form" method="post" action="/customers/update">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Add Customer</h4>
                </div>
                <div class="modal-body">

                  <div class="container-fluid row">
                    <div class="form-group col-md-6 col-lg-6">
                      <label for="form-add-name">Name</label>
                      <input type="text" name="name" class="form-control" id="form-add-name" placeholder="Name" value="{!! $customer->name !!}" required>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                      <label for="form-add-mobile">Mobile</label>
                      <input type="tel" name="mobile" class="form-control" id="form-add-mobile" placeholder="Mobile" value="{!! $customer->mobile !!}" required>
                    </div>
                    <div class="form-group col-md-12 col-lg-12">
                      <label for="form-add-mobile">Address</label>
                      <input type="text" name="address" class="form-control" id="form-add-address" placeholder="Address" value="{!! $customer->address !!}" required>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <span class="text-danger hidden error-message validation-error"><br /></span>
                  <input type="hidden" name="customerId" value="{!! $customer->customerId !!}">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" value="Save">
                  {{ csrf_field() }}
                </div>

              </form>

            </div>
          </div>
        </div>

    @endforeach

    <!-- Modal - delete -->
    @foreach ($customers as $customer)

      <div id="modal-delete-{!! $customer->customerId !!}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">

            <form method="post" action="/customers/delete">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Customer</h4>
              </div>
              <div class="modal-body">

                <p class="modal-delete-message">Are you sure you want to delete {!! $customer->name !!}?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Delete">
                <input type="hidden" name="customerId" value="{!! $customer->customerId !!}">
               {{ csrf_field() }}
              </div>

            </form>

          </div>
        </div>
      </div>

    @endforeach

@stop

@section('scripts')

  <script src="{!! URL::asset('js/customer.js') !!}"></script>

@stop
