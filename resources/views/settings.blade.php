@extends('master')

@section('content')

  <div class="jumbotron subheader">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb pull-left">
          <li>
            <p>Settings&nbsp;<i class="fa fa-gear"></i></p>
          </li>
        </ul>
        <!-- END BREADCRUMB -->
      </div>
    </div>
  </div>

  @if (session('result'))

    <div class="alert alert-{{ session('resultstatus') }} hide-later">
      {{ session('result') }}
    </div>

  @endif

  <!-- START CONTAINER FLUID -->
  <div class="container-fluid container-fixed-lg main-body">

    <form class="container-fluid row" method="post" action="/settings">

      <h5>Settings</h5>
      <div class="form-group col-md-12 col-lg-12">
        <label for="form-setting-email">Administrator</label>
        <input type="text" name="admin" class="form-control" id="form-setting-email" placeholder="Administrator" value="{!! $setting->person->name !!}" disabled>
      </div>
      <div class="form-group col-md-12 col-lg-12">
        <label for="form-setting-email">Admin Email</label>
        <input type="email" name="email" class="form-control" id="form-setting-email" placeholder="youremail@sample.com" value="{!! $setting->person->email !!}" disabled>
      </div>
      <hr class="col-lg-12" />
      <!-- <div class="form-group col-md-offset-6 col-lg-6 col-md-offset-6 col-lg-6">
        {{ csrf_field() }}
        <input type="submit" class="btn btn-primary pull-right" id="form-setting-submit" value="Save">
      </div> -->
    </form>

  </div>

@stop
